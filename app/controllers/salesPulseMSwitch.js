// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

var state = "state1";
var parentScreen;

function init(_args) {
	parentScreen = _args.parentScreen;
};

exports.init = init;

function toggleSwitchState(_state) {

	if( _state == "state1" )
		changeToState1();
	else if( _state == "state2" )
		changeToState2();
	else if( state == "state1" )
		changeToState2();
	else if( state == "state2" )
		changeToState1();
	
	Ti.API.trace("[salesPulseMSwitch] >> [toggleSwitchState] >> " + state);
	
	parentScreen.toggleSwitchState(state);
};

exports.toggleSwitchState = toggleSwitchState;

function changeToState1() {
	
	state = "state1";

	$.viewCircle.animate({
		left : 33,
		//backgroundColor : '#3DB9D5',
		//borderRadius : 6
	}, function() {
		//$.viewCircle.backgroundColor = '#BDC0C2';
		//$.viewCircle.borderRadius = 6;
	});

	$.viewLine.animate({
		left : 5
	}); 
}

function changeToState2() {
	
	state = "state2";

	$.viewCircle.animate({
		left : 5,
		//backgroundColor : '#3DB9D5',
		//borderRadius : 6
	}, function() {
		//$.viewCircle.backgroundColor = '#BDC0C2';
		//$.viewCircle.borderRadius = 6;
	});

	$.viewLine.animate({
		left : 27
	}); 
}
