var width = 400;
var height = 400;
var progress = 65;

var makeVisible = progress == 0 ? false : true;

Highcharts.chart('container', {

	chart : {
		type : 'solidgauge',
		spacing : [0, 0, 0, 0]
	},

	title : {
		text : null
	},

	tooltip : {
		enabled : false
	},

	credits : {
		enabled : false
	},

 	pane : {
	  startAngle : -150,
	  endAngle : 150,
	  background : [{// Empty Track
	    outerRadius : '105%',
      borderColor : '#FFFFFF',
	    backgroundColor : '#FFFFFF'
	  }]
	},

	yAxis : {
		min : 0,
		max : 100,
		lineWidth : 0,
		tickPositions : []
	},

	series : [{
  	animation : false,
		borderColor : '#CCCCCC',
		borderWidth : width * 0.04,
		linecap : 'round',
		data : [{
			radius : 101,
			innerRadius : 99,
			y : 100
		}],
		dataLabels : {
			enabled : false
		},
		visible : makeVisible
	}, {
		borderColor : '#64B2C8',
		borderWidth : width * 0.06,
		linecap : 'round',
		data : [{
			radius : 100,
			innerRadius : 99,
			y : progress
		}],
		dataLabels : {
			enabled : false
		},
		visible : makeVisible
	}]
});

